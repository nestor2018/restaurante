import Slider from './slider';
import elements from './elements';

let sliderText = document.querySelector("#slider-text");
let sliderTitulo = document.querySelector("#slider-titulo");
let sliderSubtitulo = document.querySelector("#slider-subtitulo");
let sliderImage = document.querySelector("#slider-image");
let textContent = document.querySelector("#slider-text-content");
let leftArrow = document.querySelector(".left-arrow");
let rightArrow = document.querySelector(".right-arrow");

let slider = new Slider({
	elements,
	animationFunc: function(element){

		textContent.classList.add("hide");
		sliderImage.classList.add("hide");

		setTimeout(function(){

		sliderTitulo.innerHTML = element.titulo;
		sliderSubtitulo.innerHTML = element.subtitulo;
		sliderText.innerHTML = element.text;
		sliderImage.src = element.image;

		textContent.classList.remove("hide");
		sliderImage.classList.remove("hide");

		},600);

	
	},
	speed: 5000
});

slider.play();

leftArrow.addEventListener('click',slider.prev);
rightArrow.addEventListener('click',slider.next);