const elements = [
		{
			titulo: 'Lorem Ipsum',
		 	subtitulo: 'Ipsum',
		 	image: '../../../public/images/4.jpg',
		 	text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum facere excepturi, laboriosam commodi sequi aliquam alias, ex, illum corporis voluptatibus velit impedit quam! Quaerat repellat aperiam impedit et. Aut, recusandae!'
		 },
		 {
			titulo: 'Lorem Ipsum 4',
		 	subtitulo: 'Ipsum 4',
		 	image: '../../../public/images/5.jpg',
		 	text: '4 Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum facere excepturi, laboriosam commodi sequi aliquam alias, ex, illum corporis voluptatibus velit impedit quam! Quaerat repellat aperiam impedit et. Aut, recusandae!'
		 },
		 {
			titulo: 'Lorem Ipsum 2',
		 	subtitulo: 'Ipsum 2',
		 	image: '../../../public/images/6.jpg',
		 	text: '2 Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum facere excepturi, laboriosam commodi sequi aliquam alias, ex, illum corporis voluptatibus velit impedit quam! Quaerat repellat aperiam impedit et. Aut, recusandae!'
		 },
		 {
			titulo: 'Lorem Ipsum 3',
		 	subtitulo: 'Ipsum 3',
		 	image: '../../../public/images/7.jpg',
		 	text: '3 Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum facere excepturi, laboriosam commodi sequi aliquam alias, ex, illum corporis voluptatibus velit impedit quam! Quaerat repellat aperiam impedit et. Aut, recusandae!'
		 }
	]; 

	export default elements;